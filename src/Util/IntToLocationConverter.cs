 using System;
using System.Buffers;
using System.Buffers.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using miniapi.src.Entity;
using Microsoft.EntityFrameworkCore;

namespace miniapi.src.Util;

public class IntToLocationConverter : JsonConverter<Location>
{
    protected ApplicationContext db;

    public IntToLocationConverter()
    {
        this.db = new ApplicationContext();
    }
    protected DbSet<Location> GetDbSet()
    {
        return this.db.Locations;
    }

    public override Location Read(ref Utf8JsonReader reader, Type type, JsonSerializerOptions options)
    {
        if (reader.TokenType == JsonTokenType.Number)
        {
            Int32 id = reader.GetInt32();
            Location location = this.GetDbSet().Find((int)id);
            
            return location;
        }

        throw new Exception();
    }

    public override void Write(Utf8JsonWriter writer, Location value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString());
    }
}
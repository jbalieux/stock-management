using miniapi.src.Entity;

namespace miniapi.src.DTO;

public class ArticleOutputDTO
{
    public string Uuid { get;}
    public string Label { get; }
    public string Description { get; }
    public Guid? LocationUuid { get; }
    public Guid? CategoryUuid { get; }


    public ArticleOutputDTO(Article article)
    {
        this.Uuid = article.Uuid.ToString();
        this.Label = article.Label;
        this.Description = article.Description;
        this.LocationUuid = article.Location?.Uuid;
        this.CategoryUuid = article.Category?.Uuid;
    }

}
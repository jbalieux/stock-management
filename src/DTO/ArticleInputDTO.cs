namespace miniapi.src.DTO;

public class ArticleInputDTO
{
    public string Label { get; set; }
    public string Description { get; set; }
    public Guid? LocationUuid { get; set; }

    public Guid? CategoryUuid { get; set; }

}
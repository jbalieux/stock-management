using miniapi.src.Entity;

namespace miniapi.src.DTO;

public class LocationOutputDTO
{
    public Guid Uuid { get;}
    public string Name { get; set; }

    public LocationOutputDTO(Location category) {
        this.Uuid = category.Uuid;
        this.Name = category.Name;
    }

}
using miniapi.src.Enum;

namespace miniapi.src.DTO;

public class MovementInputDTO
{

    public int Quantity { get; set; }

    public string Type { get; set; }

    public Guid ArticleUuid { get; set; }

    public MovementType GetType()
    {
        return Type == "input" ? MovementType.Input : Type == "output" ? MovementType.Output : throw new Exception("Not valid type");
    }

}
using miniapi.src.Entity;
using miniapi.src.Enum;

namespace miniapi.src.DTO;

public class MovementOutputDTO
{
    public Guid Uuid { get; set; } = System.Guid.NewGuid();
    
    public int Quantity { get; set; }

    public string Type { get; set; }
    
    public Guid ArticleGuid { get; set; }

    public MovementOutputDTO(Movement entity) {
        this.Uuid = entity.Uuid;
        this.Quantity = entity.Quantity;
        this.Type = entity.Type == MovementType.Input ? "input" : entity.Type == MovementType.Output ? "output" : throw new Exception("Not valid type");
        this.ArticleGuid = entity.Article.Uuid;
    }

}
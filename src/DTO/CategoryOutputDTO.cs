using miniapi.src.Entity;

namespace miniapi.src.DTO;

public class CategoryOutputDTO
{
    public Guid Uuid { get;}
    public string Name { get; set; }

    public CategoryOutputDTO(Category category) {
        this.Uuid = category.Uuid;
        this.Name = category.Name;
    }

}
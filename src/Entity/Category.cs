using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace miniapi.src.Entity;

public class Category : IEntity
{
    [Key]
    public int Id { get; set; }
    public Guid Uuid { get; set; } = System.Guid.NewGuid();
    public string Name { get; set; }
}
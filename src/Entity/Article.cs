using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using miniapi.src.Util;

namespace miniapi.src.Entity;

public class Article
{
    [Key]
    public int Id { get; set; }

    public Guid Uuid { get; set; } = System.Guid.NewGuid();
    public string Label { get; set; }

    public string Description { get; set; }
    
    public virtual Location? Location { get; set; }

    public Category? Category { get; set; }
}
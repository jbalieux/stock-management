using System.ComponentModel.DataAnnotations;
using miniapi.src.Enum;

namespace miniapi.src.Entity;

public class Movement
{
    [Key]
    public int Id { get; set; }

    public Guid Uuid { get; set; } = System.Guid.NewGuid();
    
    public int Quantity { get; set; }

    public MovementType Type { get; set; }
    
    public Article Article { get; set; }
}
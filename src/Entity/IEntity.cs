namespace miniapi.src.Entity;

abstract public class IEntity
{
    public int Id {get; set;}

    public Guid Uuid {get; set;}
}
using Microsoft.EntityFrameworkCore;

namespace miniapi.src.Entity;

public class ApplicationContext : DbContext
{
    public DbSet<Location> Locations { get; set; }

    public DbSet<Category> Categories { get; set; }

    public DbSet<Article> Articles { get; set; }

    public DbSet<Movement> Movements { get; set; }

    public string DbPath { get; }

    public ApplicationContext()
    {
        var folder = Environment.CurrentDirectory;
        DbPath = System.IO.Path.Join(folder, "var", "application.db");
    }

    // The following configures EF to create a Sqlite database file in the
    // special "local" folder for your platform.
    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite($"Data Source={DbPath}");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // modelBuilder.Entity<Article>()
        //     .HasOne(c => c.Location)
        //     .WithMany(e => e.Articles)
        //     .HasForeignKey(p => p.LocationId);

    }
}
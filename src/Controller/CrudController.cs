using miniapi.src.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using miniapi.src.Util;

namespace miniapi.src.Controller;

public abstract class CrudController<T, U> : ControllerBase where T : IEntity where U : class
{
    protected ApplicationContext db;

    public CrudController()
    {
        db = new ApplicationContext();
    }

    abstract protected DbSet<T> GetDbSet();

    [HttpGet]
    public IEnumerable<U> GetAll()
    {
        return GetDbSet()
            .ToList()
            .Select(entity => ApplyDTO(entity));
    }

    [HttpGet("{uuid}")]
    public ActionResult<U> GetOne(Guid uuid)
    {
        var entity = GetDbSet().First(x => x.Uuid == uuid);
        if (entity == null)
        {
            return NotFound();
        }
        return ApplyDTO(entity);
    }

    [HttpPost]
    public U Post(T entity)
    {
        GetDbSet().Add(entity);
        db.SaveChanges();
        return ApplyDTO(entity);
    }

    [HttpPut("{uuid}")]
    public ActionResult<U> Put(Guid uuid, T entity)
    {
        var foundEntity = GetDbSet().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        Reflection.CopyProperties(entity, foundEntity);
        db.SaveChanges();
        return ApplyDTO(foundEntity);
    }

    
    [HttpPatch("{uuid}")]
    public ActionResult<U> Patch(Guid uuid, T entity)
    {
        var foundEntity = GetDbSet().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        Reflection.CopyProperties(entity, foundEntity);
        db.SaveChanges();
        return ApplyDTO(foundEntity);
    }

    [HttpDelete("{uuid}")]
    public ActionResult Delete(Guid uuid)
    {
        var foundEntity = GetDbSet().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        db.Remove(foundEntity);
        db.SaveChanges();
        return NoContent();
    }

    private U ApplyDTO(T entity)
    {
        return (U)Activator.CreateInstance(typeof(U), new object[] { entity });
    }
}

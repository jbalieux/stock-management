using miniapi.src.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using miniapi.src.DTO;
using Microsoft.EntityFrameworkCore.Query;
using miniapi.src.Util;

namespace miniapi.src.Controller;

[ApiController]
[Route("api/movements")]
public class MovementController : ControllerBase
{
    private ApplicationContext db;

    public MovementController()
    {
        db = new ApplicationContext();
    }

    protected DbSet<Movement> GetDbSet()
    {
        return db.Movements;
    }

    protected IIncludableQueryable<Movement, Article?> GetDbSetEager()
    {
        return GetDbSet()
            .Include(x => x.Article);
    }

    [HttpGet]
    public IEnumerable<MovementOutputDTO> GetAll()
    {
        return GetDbSetEager()
            .ToList()
            .Select(x => new MovementOutputDTO(x));
    }

    
    [HttpGet("{uuid}")]
    public ActionResult<MovementOutputDTO> GetOne(Guid uuid)
    {
        var foundEntity = GetDbSetEager().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        return new MovementOutputDTO(foundEntity);
    }

    [HttpPost]
    public MovementOutputDTO Post(MovementInputDTO dto)
    {
        Article? article = db.Articles.First(l => l.Uuid == dto.ArticleUuid);
        Movement movement = new Movement {
            Quantity = dto.Quantity,
            Type = dto.GetType(),
            Article = article
        };
        GetDbSet().Add(movement);
       
        db.SaveChanges();
        return new MovementOutputDTO(movement);
    }

    [HttpPut("{uuid}")]
    public ActionResult<MovementOutputDTO> Put(Guid uuid, MovementInputDTO dto)
    {
        Article? article = db.Articles.First(l => l.Uuid == dto.ArticleUuid);
        Movement movement = new Movement {
            Quantity = dto.Quantity,
            Type = dto.GetType(),
            Article = article
        };
        var foundEntity = GetDbSet().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        Reflection.CopyProperties(movement, foundEntity);
        db.SaveChanges();
        return new MovementOutputDTO(movement);
    }

    [HttpPatch("{uuid}")]
    public ActionResult<MovementOutputDTO> Patch(Guid uuid, MovementInputDTO dto)
    {
        Article? article = db.Articles.First(l => l.Uuid == dto.ArticleUuid);
        Movement movement = new Movement {
            Quantity = dto.Quantity,
            Type = dto.GetType(),
            Article = article
        };
        var foundEntity = GetDbSet().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        Reflection.CopyProperties(movement, foundEntity);
        db.SaveChanges();
        return new MovementOutputDTO(movement);
    }

    [HttpDelete("{uuid}")]
    public ActionResult<MovementOutputDTO> Delete(Guid uuid)
    {
        Movement? foundEntity = GetDbSet().FirstOrDefault(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        db.Remove(foundEntity);
        db.SaveChanges();
        return NoContent();
    }

}

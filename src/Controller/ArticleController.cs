using miniapi.src.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using miniapi.src.DTO;
using Microsoft.OpenApi.Any;
using Microsoft.EntityFrameworkCore.Query;
using miniapi.src.Util;

namespace miniapi.src.Controller;

[ApiController]
[Route("api/articles")]
public class ArticleController : ControllerBase
{
    private ApplicationContext db;

    public ArticleController()
    {
        db = new ApplicationContext();
    }

    protected DbSet<Article> GetDbSet()
    {
        return db.Articles;
    }

    protected IIncludableQueryable<Article, Category?> GetDbSetEager()
    {
        return GetDbSet()
            .Include(x => x.Location)
            .Include(x => x.Category);
    }

    [HttpGet]
    public IEnumerable<ArticleOutputDTO> GetAll()
    {
        return GetDbSetEager()
            .ToList()
            .Select(x => new ArticleOutputDTO(x));
    }

    
    [HttpGet("{uuid}")]
    public ActionResult<ArticleOutputDTO> GetOne(Guid uuid)
    {
        var foundEntity = GetDbSetEager().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        return new ArticleOutputDTO(foundEntity);
    }

    [HttpPost]
    public ArticleOutputDTO Post(ArticleInputDTO dto)
    {
        Location? location = dto.LocationUuid.HasValue ? db.Locations.First(l => l.Uuid == dto.LocationUuid) : null;
        Category? category = dto.CategoryUuid.HasValue ? db.Categories.First(l => l.Uuid == dto.CategoryUuid) : null;
        Article article = new Article {
            Label = dto.Label,
            Description = dto.Description,
            Location = location,
            Category = category
        };
        GetDbSet().Add(article);
       
        db.SaveChanges();
        return new ArticleOutputDTO(article);
    }

    [HttpPut("{uuid}")]
    public ActionResult<ArticleOutputDTO> Put(Guid uuid, ArticleInputDTO dto)
    {
        Location? location = dto.LocationUuid.HasValue ? db.Locations.First(l => l.Uuid == dto.LocationUuid) : null;
        Category? category = dto.CategoryUuid.HasValue ? db.Categories.First(l => l.Uuid == dto.CategoryUuid) : null;
        Article article = new Article {
            Label = dto.Label,
            Description = dto.Description,
            Location = location,
            Category = category
        };
        var foundEntity = GetDbSet().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        Reflection.CopyProperties(article, foundEntity);
        db.SaveChanges();
        return new ArticleOutputDTO(article);
    }

    [HttpPatch("{uuid}")]
    public ActionResult<ArticleOutputDTO> Patch(Guid uuid, ArticleInputDTO dto)
    {
        Location? location = dto.LocationUuid.HasValue ? db.Locations.First(l => l.Uuid == dto.LocationUuid) : null;
        Category? category = dto.CategoryUuid.HasValue ? db.Categories.First(l => l.Uuid == dto.CategoryUuid) : null;
        Article article = new Article {
            Label = dto.Label,
            Description = dto.Description,
            Location = location,
            Category = category
        };
        var foundEntity = GetDbSet().First(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        Reflection.CopyProperties(article, foundEntity);
        db.SaveChanges();
        return new ArticleOutputDTO(article);
    }

    [HttpDelete("{uuid}")]
    public ActionResult<ArticleOutputDTO> Delete(Guid uuid)
    {
        Article? foundEntity = GetDbSet().FirstOrDefault(x => x.Uuid == uuid);
        if (foundEntity == null)
        {
            return NotFound();
        }
        db.Remove(foundEntity);
        db.SaveChanges();
        return NoContent();
    }

}

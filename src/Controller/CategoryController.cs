using miniapi.src.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using miniapi.src.DTO;

namespace miniapi.src.Controller;

[ApiController]
[Route("api/categories")]
public class CategoryController : CrudController<Category, CategoryOutputDTO>
{
    protected override DbSet<Category> GetDbSet()
    {
        return db.Categories;
    }
}
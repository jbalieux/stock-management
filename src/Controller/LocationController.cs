using miniapi.src.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using miniapi.src.DTO;

namespace miniapi.src.Controller;

[ApiController]
[Route("api/locations")]
public class LocationController : CrudController<Location, LocationOutputDTO>
{
    protected override DbSet<Location> GetDbSet()
    {
        return db.Locations;
    }
}